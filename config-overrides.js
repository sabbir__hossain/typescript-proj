module.exports = {
  webpack: (config, env) => {
    const newConfig = config;
    newConfig.optimization.runtimeChunk = false;
    newConfig.optimization.splitChunks = {
      cacheGroups: {
        default: false,
      },
    };

    newConfig.output.filename = "static/js/[name].js";

    newConfig.plugins[5].options.filename = "static/css/[name].css";
    newConfig.plugins[5].options.moduleFilename = () => "static/css/main.css";
    return newConfig;
  },
};
