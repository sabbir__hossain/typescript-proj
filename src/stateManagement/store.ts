// /* eslint-disable import/no-extraneous-dependencies */
// import { applyMiddleware, createStore } from "redux";
// import { configureStore } from "@reduxjs/toolkit";
// import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
// import { persistReducer, persistStore } from "redux-persist";
// import storage from "redux-persist/lib/storage";
// import createSagaMiddleware from "redux-saga";
// import rootReducer from "./rootReducer";
// import rootSaga from "./rootSaga";

// const sagaMiddleware = createSagaMiddleware();

// let persistConfig;

// if (process.env.REACT_APP_REDUX_PERSIST_DATA === "true") {
//     persistConfig = {
//         key: "root",
//         storage,
//         whitelist: []
//     };
// } else {
//     persistConfig = {
//         key: "root",
//         storage,
//         whitelist: []
//     };
// }

// const persistedReducer = persistReducer(persistConfig, rootReducer);

// const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));

// const persistor = persistStore(store);

// sagaMiddleware.run(rootSaga);

// export { store, persistor };

// import { configureStore } from "@reduxjs/toolkit";
// import { persistReducer, persistStore } from "redux-persist";
// import storage from "redux-persist/lib/storage";
// import createSagaMiddleware from "redux-saga";
// import rootReducer from "./rootReducer";
// import rootSaga from "./rootSaga";

// const sagaMiddleware = createSagaMiddleware();

// const persistConfig = {
//     key: "root",
//     storage,
//     whitelist: ["auth"]
// };

// const persistedReducer = persistReducer(persistConfig, rootReducer);

// const store = configureStore({
//     reducer: persistedReducer,
//     middleware: [sagaMiddleware]
// });

// const persister = persistStore(store);
// sagaMiddleware.run(rootSaga);
// // Infer the `RootState` and `AppDispatch` types from the store itself
// export type RootState = ReturnType<typeof store.getState>;
// // Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
// export type AppDispatch = typeof store.dispatch;

// export { store, persister };

import { applyMiddleware, createStore } from "redux";
// eslint-disable-next-line import/no-extraneous-dependencies
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./rootReducer";
import rootSaga from "./rootSaga";

const sagaMiddleware = createSagaMiddleware();

let persistConfig;

if (process.env.REACT_APP_REDUX_PERSIST_DATA === "true") {
    persistConfig = {
        key: "root",
        storage,
        whitelist: []
    };
} else {
    persistConfig = {
        key: "root",
        storage,
        whitelist: []
    };
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };
