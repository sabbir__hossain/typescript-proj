import { CustomizedMenus } from "@hipdevteam/afglobalcomponents";
import MenuIcon from "@mui/icons-material/Menu";
import { AppBar, Box, IconButton, StyledEngineProvider, Toolbar, useTheme } from "@mui/material";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import GHLReportingDarkLogo from "../../../assets/images/logo/ghl-reporting-logo-dark.png";
import GHLReportingLightLogo from "../../../assets/images/logo/ghl-reporting-logo-light.png";
import * as CONSTANT from "../../../utils/Constant/GlobalConstant";

function TopBar({ mobileOpen, setMobileOpen, apps }) {
    // Please assign your default style classes which are include in style file
    const customTheme = useTheme();
    const [appList, setAppList] = useState([]);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    useEffect(() => {
        if (apps.isSuccess) {
            setAppList(apps.apps);
        }
    }, [apps.isSuccess]);

    return (
        <StyledEngineProvider injectFirst>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar
                    color="background"
                    position="fixed"
                    sx={{
                        width: "100%",

                        boxShadow: "0px 1px 0px rgba(42, 46, 52, 0.12)",
                        backgroundImage: "none"
                    }}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            edge="start"
                            onClick={handleDrawerToggle}
                            sx={{ mr: 2, display: { lg: "none" } }}
                        >
                            <MenuIcon sx={{ color: "#2578ff" }} />
                        </IconButton>

                        <CustomizedMenus
                            sx={{ marginRight: "24px" }}
                            appList={appList}
                            appUrls={CONSTANT.appUrls}
                        />

                        <Box sx={{ display: "inline-flex" }}>
                            {customTheme.palette.mode === "dark" ? (
                                <img src={GHLReportingDarkLogo} alt="logo" />
                            ) : (
                                <img src={GHLReportingLightLogo} alt="logo" />
                            )}
                        </Box>
                    </Toolbar>
                </AppBar>
            </Box>
        </StyledEngineProvider>
    );
}

const mapStateToProps = (state) => {
    return {
        userProfileData: state.userProfileManageReducer.userProfileData,
        apps: state.appReducer
    };
};
export default connect(mapStateToProps, null)(TopBar);
