export const STATUS_QUEUED = 0;
export const STATUS_PROCESSING = 1;
export const STATUS_PROCESSED = 2;
export const TYPE_GHL_SYNC = 1;
export const TYPE_BULK_INACTIVE = 2; // Client
export const TYPE_BULK_DELETE = 3; // Client
