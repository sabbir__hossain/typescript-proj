import storage from "local-storage-fallback";
import React, { useEffect, useState } from "react";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import UserLayout from "../components/layouts/UserLayout";
import { getCookie } from "../helpers/Cookie";
import { UserLayoutRouterProps } from "../interfaces/routes/UserLayoutRouter";

function UserLayoutRouter(props: UserLayoutRouterProps) {
    const { appName, haveAccess, pageTitle } = props;
    const [isAuthenticatedUser, setIsAuthenticatedUser] = useState(true);
    const navigate = useNavigate();
    const location = useLocation();
    const appRedirectTo: string = process.env.REACT_APP_REDIRECT_TO || "";
    const appRefreshToken: string = process.env.REACT_APP_REFRESH_TOKEN || "";
    const appEmailVerifiedStatus: string = process.env.REACT_APP_EMAIL_VERIFIED_STATUS || "";
    storage.setItem(appRedirectTo, location.pathname);

    useEffect(() => {
        if (getCookie(appRefreshToken) !== "") {
            if (getCookie(appEmailVerifiedStatus) === "false") {
                navigate("/user/email-verification");
            } else {
                setIsAuthenticatedUser(true);
            }
        } else {
            navigate("/login");
        }
    }, []);
    const [count, setCount] = useState(10);
    return isAuthenticatedUser ? (
        <HelmetProvider>
            {pageTitle && (
                <Helmet defer={false}>
                    <title>{pageTitle}</title>
                </Helmet>
            )}
            <UserLayout pageTitle="Test" appName={appName} haveAccess={haveAccess}>
                <Outlet context={[count, setCount]} />
            </UserLayout>
        </HelmetProvider>
    ) : null;
}
export default UserLayoutRouter;
