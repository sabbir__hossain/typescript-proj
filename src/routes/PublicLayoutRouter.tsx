// import React from "react";
// import { Route } from "react-router-dom";
// import PublicLayout from "../components/layouts/PublicLayout";
// import { RoutesProps } from "../interfaces/routes/Common";

// function PublicLayoutRouter(props: RoutesProps) {
//     const { component: Component, pageTitle, path } = props;
//     return (
//         <Route
//             path={path}
//             element={
//                 <PublicLayout pageTitle={pageTitle}>
//                     <Component {...props} />
//                 </PublicLayout>
//             }
//         />
//     );
// }

// export default PublicLayoutRouter;

import React from "react";

function PublicLayoutRouter() {
    return <div>PublicLayoutRouter</div>;
}

export default PublicLayoutRouter;
