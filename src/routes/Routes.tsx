import React, { useEffect, useState } from "react";
import { Route, Routes as Switch } from "react-router-dom";
import { ReducerStateProps } from "../interfaces/routes/Routes";
import Test from "../pages/Test";
import UserLayoutRouter from "./UserLayoutRouter";

type RouteProps = {
    featureAccess: {
        loading: boolean;
    };
};

function Routes(props: RouteProps) {
    const { featureAccess } = props;
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 1000);
    }, [featureAccess.loading]);

    // const renderRouter = () => {
    //     if (isLoading) {
    //         return "<MainPageSkeleton />";
    //     }
    //     return (
    //         <Switch>
    //             <Route path="/check" element={<Test />} />
    //             <Route path="/login" element={<Test />} />
    //             {/* <Route
    //                 path="/*"
    //                 element={<UserLayoutRouter appName="ad-local" haveAccess={true} />}
    //             >
    //                 <Route path="test" element={<Test />} />
    //             </Route> */}
    //             {/* GHL  Opportunity Report Create */}.
    //             {/* <UserLayoutRouter
    //                 path="/test"
    //                 pageTitle="GHL Report | Opportunity"
    //                 appName={CONSTANT.MAIN_SIDEBAR}
    //                 haveAccess
    //             />
    //             <ErrorLayoutRouter
    //                 path="*"
    //                 component={NotFound}
    //                 pageTitle="404 Not Found"
    //                 appName=""
    //             /> */}
    //         </Switch>
    //     );
    // };

    return (
        <Switch>
            <Route path="/check" element={<Test />} />
            <Route path="/login" element={<Test />} />
            <Route
                path="/*"
                element={<UserLayoutRouter appName="ad-local" haveAccess={true} pageTitle="Test" />}
            >
                <Route path="test" element={<Test />} />
            </Route>

            {/* <Route path="/*" element={<ErrorLayoutRouter />}>
                <Route path="test" element={<NotFound />} />
            </Route> */}
        </Switch>
    );
}

const mapStateToProps = (state: ReducerStateProps) => ({
    featureAccess: state.featureAccessReducer
});

export default Routes;
