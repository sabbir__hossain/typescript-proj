import socketIOClient from "socket.io-client";

const serverEndpoint = process.env.REACT_APP_API_BASE_URL;

// eslint-disable-next-line import/prefer-default-export
export const socket = socketIOClient(serverEndpoint);
