export interface ReducerStateProps {
    featureAccessReducer: object;
}

// /* eslint-disable consistent-return */
// import storage from "local-storage-fallback";
// import React from "react";
// import { Redirect, Route, useLocation } from "react-router-dom";
// import PageWrapper from "../components/layouts/UserLayout";
// import { getCookie } from "../helpers/Cookie";

// function UserLayoutRouter(
//     { component: Component, pageTitle, appName, haveAccess = true, ...rest },
//     props
// ) {
//     const location = useLocation();
//     storage.setItem(process.env.REACT_APP_REDIRECT_TO, location.pathname);

//     return (
//         <Route
//             {...rest}
//             render={() => {
//                 if (getCookie(process.env.REACT_APP_REFRESH_TOKEN) !== "") {
//                     if (
//                         getCookie(process.env.REACT_APP_EMAIL_VERIFIED_STATUS) === "false" ||
//                         process.env.REACT_APP_EMAIL_VERIFIED_STATUS === false
//                     ) {
//                         return <Redirect to="/user/email-verification" />;
//                     }
//                     return (
//                         <PageWrapper
//                             pageTitle={pageTitle}
//                             appName={appName}
//                             haveAccess={haveAccess}
//                         >
//                             <Component {...props} />
//                         </PageWrapper>
//                     );
//                 }
//                 window.location.replace("/login");
//             }}
//         />
//     );
// }

// export default UserLayoutRouter;
