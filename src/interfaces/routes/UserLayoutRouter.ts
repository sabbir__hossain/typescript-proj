export interface UserLayoutRouterProps {
    appName: string;
    haveAccess: boolean;
    pageTitle: string;
}
