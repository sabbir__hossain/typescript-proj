export interface UserProfileDataType {
    id: number;
    uuid: string;
    first_name: string;
    last_name: string;
    email: string;
    status: number;
    role: number;
    email_verified: number;
    default_user: number;
    agency_id: number;
    address: string;
    phone_number: string;
    google_id?: string;
    is_super_admin: number;
    created_at: string;
    updated_at: string;
}

export interface FeatureAccessType {
    id: number;
}
