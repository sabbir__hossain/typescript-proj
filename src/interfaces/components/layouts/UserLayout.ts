import { FeatureAccessType, UserProfileDataType } from "../../Common";

export interface UserLayoutProps {
    children: React.ReactNode;
    pageTitle: string;
    userProfileData?: UserProfileDataType;
    customClass?: string;
    appName: string;
    haveAccess: boolean;
    featureAccess?: FeatureAccessType;
}
