export interface TopBarProps {
    mobileOpen: boolean;
    setMobileOpen: Function;
    apps?: any;
}
