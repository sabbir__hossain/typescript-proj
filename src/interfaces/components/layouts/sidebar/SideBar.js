

export interface SideBarProps {
    drawerWidth:number;
    mobileOpen:boolean;
    setMobileOpen: Function;
    ThemeToggleButton: Function;
    customClass?:string;
    appName?:string
    userProfileData?: import("../../../Common").UserProfileDataType;
    featureAccess?:any
}