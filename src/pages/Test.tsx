/* eslint-disable react/button-has-type */
import { CustomButton, CustomLabel } from "@hipdevteam/afglobalcomponents";
import React from "react";

function Test() {
    return (
        <div>
            <CustomLabel>label</CustomLabel>
            <CustomButton title="button" color="secondary" />
        </div>
    );
}

export default Test;
