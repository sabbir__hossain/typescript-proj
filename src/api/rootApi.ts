import axios from "axios";
import { getCookie } from "../helpers/Cookie";

const accessToken: string = process.env.REACT_APP_ACCESS_TOKEN || "";
export const sendRequest = (method: string, url: string, payload: object = {}) =>
    axios.request({
        method,
        url,
        data: payload
    });

export const sendBlobTypeRequest = (method: string, url: string, payload: object = {}) => {
    // For Cancel Token
    let cancelToken: any = {};
    if (typeof cancelToken !== typeof undefined) {
        cancelToken.cancel("Canceling the previous token.");
    }
    cancelToken = axios.CancelToken.source();

    const bearerToken = getCookie(accessToken);

    return axios.request({
        method,
        url,
        responseType: "blob",
        data: payload,
        headers: {
            Authorization: `Bearer ${bearerToken}`
        },
        cancelToken: cancelToken.token
    });
};

export const sendRequestWithToken = (method: string, url: string, payload: object = {}) => {
    // For Cancel Token
    let cancelToken: any;

    if (typeof cancelToken !== typeof undefined) {
        cancelToken.cancel("Canceling the previous token.");
    }

    cancelToken = axios.CancelToken.source();

    const bearerToken = getCookie(accessToken);

    return axios.request({
        method,
        url,
        data: payload,
        headers: {
            Authorization: `Bearer ${bearerToken}`
        },
        cancelToken: cancelToken.token
    });
};
