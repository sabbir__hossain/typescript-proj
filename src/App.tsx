/* eslint-disable no-unused-vars */
import { StylesProvider } from "@mui/styles";
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "./App.css";
import Routes from "./routes/Routes";

function App(props: {}) {
    return (
        <Router>
            <StylesProvider>
                <ToastContainer />
                <Routes
                    featureAccess={{
                        loading: false
                    }}
                />
            </StylesProvider>
        </Router>
    );
}

export default App;
