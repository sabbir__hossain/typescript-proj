import { isEmptyObject, isObject } from "./helperFunctions";

const emptyFieldValidator = (field) => {
    if (isObject(field) && isEmptyObject(field)) {
        console.log("empty object");
        return false;
    }
    if (Array.isArray(field) && field.length === 0) {
        console.log("empty array");
        return false;
    }
    if (field === "" || field == undefined || field == null) {
        console.log("empty string");
        return false;
    }

    return true;
};
export default emptyFieldValidator;
