/* eslint-disable import/prefer-default-export */
export const s3ConfigForClientListCSV = {
    bucketName: process.env.REACT_APP_S3_BUCKET,
    dirName: "client_csv_upload" /* optional */,
    region: process.env.REACT_APP_S3_REGION,
    accessKeyId: process.env.REACT_APP_S3_ACCESS_KEY,
    secretAccessKey: process.env.REACT_APP_S3_SECRET,
    s3Url: process.env.REACT_APP_S3_URL_PREFIX /* optional */,
};
