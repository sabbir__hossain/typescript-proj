export const ALERT_TYPE_SUCCESS = "ALERT_TYPE_SUCCESS";
export const ALERT_TYPE_ERROR = "ALERT_TYPE_ERROR";
export const ALERT_TYPE_INFO = "ALERT_TYPE_INFO";
export const ALERT_TYPE_WARNING = "ALERT_TYPE_WARNING";

// Credentials
export const AUTH_TYPE_GHL = 2;

export const USER_TYPE_ADMIN = 1;
export const bytesToMb = (bytes, decimals = 2) => {
    if (bytes === 0) return 0;
    const dm = decimals < 0 ? 0 : decimals;

    return parseFloat((bytes / 1048576).toFixed(dm));
};
export const getRandomFileName = (name) => {
    const timestamp = new Date().toISOString();
    const random = `${Math.random()}`.substring(2, 8);
    return `${timestamp}-${random}-${name.toString().replace(" ", "_")}`;
};

export const MAIN_SIDEBAR = "MAIN_SIDEBAR";
export const SETTING_SIDEBAR = "SETTING_SIDEBAR";
export const INTEGRATIONS_SIDEBAR = "INTEGRATIONS_SIDEBAR";

// ACCESS NAME
export const ACCESS_AF_SECRET_SHOP = "af_secret_shop";
export const ACCESS_CAMPAIGN = "Campaign";
export const ACCESS_PROSPECT = "Prospects";
export const ACCESS_SETTINGS = "Settings";

// GHL
export const GHL_STATUS_ACTIVE = 1;
export const GHL_STATUS_INACTIVE = 0;

export const GHL_STATUS_INACTIVE_PROCESSING = 2;
export const GHL_STATUS_DELETE_PROCESSING = 3;

export const GHL_STATUS_DELETE = 4;

export const GHL_OPPORTUNITY_PROCESSING = 1;
export const GHL_OPPORTUNITY_PROCESSED = 0;

export const apiAuth = {
    AUTH_TYPE_TWILIO: 0,
    AUTH_TYPE_MAILGUN: 1,
    AUTH_TYPE_GHL: 2,
    STATUS_ACTIVE: 1,
    STATUS_INACTIVE: 0,
};

export const appUrls = {
    af_schema: "/schema/projects",
    af_secret_shop: "/sstool/dashboard",
    af_ghl_report: "/ghl/location",
};
