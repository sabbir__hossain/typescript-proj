export const SELECT_FILTER_TYPE = 0;
export const TAG_FILTER = 1;
export const SOURCE_FILTER = 2;
export const OFFER_FILTER = 3;

export const SELECT_MONTHLY_VIEW = 0;
export const MONTHLY_VIEW_YES = 1;
export const MONTHLY_VIEW_NO = 2;
