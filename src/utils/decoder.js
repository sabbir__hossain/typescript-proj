const decoder = (encodedStringAtoB) => {
    const decodedStringAtoB = atob(encodedStringAtoB);
    return decodedStringAtoB.replace(/%a%/g, "");
};

export default decoder;
