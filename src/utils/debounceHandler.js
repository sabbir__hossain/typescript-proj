// Debounce Handler For example, if you want to call a function after a user has stopped typing for 1 second, you can use the debounce function.
function debounceHandler(fn, delay = 2000) {
    let timeoutId;
    return function () {
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        timeoutId = setTimeout(() => {
            fn();
        }, delay);
    };
}

export default debounceHandler;
