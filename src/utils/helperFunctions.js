/* eslint-disable no-useless-escape */

import { Typography } from "@mui/material";

export const isObject = (obj) => {
    return Object.prototype.toString.call(obj) === "[object Object]";
};

export const isEmptyObject = (obj) => {
    return Object.keys(obj).length === 0;
};

export const isValidUrl = (url) => {
    const pattern = new RegExp(
        "^(https?:\\/\\/)?" + // protocol
            "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
            "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
            "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
            "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
            "(\\#[-a-z\\d_]*)?$",
        "i"
    ); // fragment locator
    return !!pattern.test(url);
};

export const isImageUrl = (url) => {
    return String(url)
        .toLowerCase()
        .match(/data:image\/([a-zA-Z]*);base64,([^\"]*)/g);
};
export const isEmailValid = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};
export const isValidTelephone = () => {
    return true;
};
export const isValidZipCodes = (zip) => {
    return String(zip).match("^[0-9]+$");
};

export const isValidImageUrl = (url) => {
    return !!(isValidUrl(url) || isImageUrl(url));
};

export const haveAccess = (accessList, accessName) => {
    if (Array.isArray(accessList)) {
        if (accessList.includes(accessName)) {
            return true;
        }
    }
    return false;
};

export const renderErrorMessage = (error, message) => {
    if (error !== undefined) {
        return (
            <Typography variant="subtitle2" component="span" color="error">
                {message}
            </Typography>
        );
    }
    return "";
};

export const generateRandomString = (length = 10) => {
    let randomString = "";
    const CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const CHARACTERS_LENGTH = CHARACTERS.length;

    for (let i = 0; i < length; i += 1) {
        randomString += CHARACTERS.charAt(Math.floor(Math.random() * CHARACTERS_LENGTH));
    }
    return randomString;
};
export const formatPhoneNumber = (phoneNumberString) => {
    // eslint-disable-next-line no-param-reassign
    if (typeof phoneNumberString !== "string") {
        // eslint-disable-next-line no-param-reassign
        phoneNumberString = phoneNumberString.toString();
    }
    phoneNumberString = phoneNumberString.replace("+", "");
    let formattedNumber = "+";
    formattedNumber += phoneNumberString.substring(0, 1);
    formattedNumber += `(${phoneNumberString.substring(1, 4)})`;
    formattedNumber += `${phoneNumberString.substring(4, 7)}-`;
    formattedNumber += phoneNumberString.substring(7, 11);
    return formattedNumber;
};

export const getUSAreaCodeByNumber = (number) => {
    // eslint-disable-next-line no-param-reassign
    if (typeof number !== "string") {
        // eslint-disable-next-line no-param-reassign
        number = number.toString();
    }
    // eslint-disable-next-line no-param-reassign
    number = number.trim().replace("+", "");
    const firstCharacter = number.substring(0, 1);
    if (number.length === 11 && firstCharacter === "1") {
        return number.substring(1, 4);
    }
    if (number.length === 10 && firstCharacter !== "1") {
        return number.substring(0, 3);
    }
    return false;
};
