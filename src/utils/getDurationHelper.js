import { differenceInMinutes, parseISO } from "date-fns";
import { format } from "date-fns-tz";

const formatTime = (time, formatPattern = "yyyy-MM-dd hh:mm:ss") => {
    if (typeof time !== "string") {
        return format(time, formatPattern);
    }
    return format(new Date(time), formatPattern);
};

const getDuration = (executionTime, dataTime) => {
    if (dataTime === "" || dataTime === null) {
        return "";
    }
    const start = formatTime(dataTime);
    const end = formatTime(executionTime);
    const minutes = differenceInMinutes(parseISO(start), parseISO(end));
    const seconds = minutes * 60;
    const h = Math.floor(seconds / 3600);
    const m = Math.floor((seconds % 3600) / 60);
    const hDisplay = h > 0 ? h : 0;
    const mDisplay = m > 0 ? m : 0;
    let finalHour = hDisplay;
    let finalMin = mDisplay;
    if (parseInt(hDisplay, 10) < 10) {
        finalHour = `0${hDisplay}`;
    }
    if (parseInt(mDisplay, 10) < 10) {
        finalMin = `0${mDisplay}`;
    }

    return `${finalHour}:${finalMin}`;
};
export default getDuration;
