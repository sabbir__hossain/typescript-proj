import { CustomTooltip } from "@hipdevteam/afglobalcomponents";
import { Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import * as GLOBAL_CONSTANTS from "../utils/Constant/GlobalConstant";

type UserProfileDataType = {
    role: number;
};

type RowDataType = {
    created_by: string;
};

interface PropsInterface {
    row: RowDataType;
    userProfileData: UserProfileDataType;
    prefix?: string;
}

function CreatedBy(props: PropsInterface) {
    const { row, userProfileData, prefix = "Created By" } = props;
    const [userRole, setUserRole] = useState(0);

    useEffect(() => {
        if (userProfileData) {
            setUserRole(userProfileData.role);
        }
    }, [userProfileData]);
    const renderText = (text: string) => {
        if (text.length <= 12) {
            return text;
        }

        return `${text.substring(0, 12)}...`;
    };

    if (userRole === GLOBAL_CONSTANTS.USER_TYPE_ADMIN) {
        return (
            <CustomTooltip title={row.created_by} placement="top">
                <Typography variant="body2" color="action.active">
                    {prefix} {renderText(row.created_by)}
                </Typography>
            </CustomTooltip>
        );
    }
    return null;
}

export default connect(null, null)(CreatedBy);
