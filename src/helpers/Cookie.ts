const timeToMiliSecond = (time: string) => {
    const result: string = time.substring(0, time.indexOf(" "));
    const finalResult: number = parseInt(result, 10);
    switch (time.substring(time.indexOf(" ") + 1)) {
        case "day":
            return finalResult * 24 * 60 * 60 * 1000;
        case "hour":
            return finalResult * 60 * 60 * 1000;
        case "minute":
            return finalResult * 60 * 1000;
        case "second":
            return finalResult * 1000;
        default:
            return finalResult * 60 * 1000;
    }
};

export const setCookie = (cname: string, cvalue: string, exTime: any) => {
    const date = new Date();
    date.setTime(date.getTime() + timeToMiliSecond(exTime));

    document.cookie = `${cname}=${cvalue}; expires = ${date.toUTCString()};path=/`;
    // document.cookie = cname + "=" + cvalue + "; Secure; path=/";
};

export const setOtherCookie = (cname: string, cvalue: string) => {
    document.cookie = `${cname}=${cvalue}`;
};

export const setCookieForRefresh = (cname: string, cvalue: string) => {
    document.cookie = `${cname}=${cvalue}; Secure; path=/`;
};

export const getCookie = (cname: string) => {
    const name = `${cname}=`;
    const ca = document.cookie.split(";");
    for (let i = 0; i < ca.length; i += 1) {
        let c = ca[i];
        while (c.charAt(0) === " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

const parseJwt = (token: string) => {
    try {
        return JSON.parse(atob(token.split(".")[1]));
    } catch (e) {
        return null;
    }
};

export const checkCookie = (cname: string) => {
    const cookieInfo = getCookie(cname);

    if (cookieInfo === "") {
        return "";
    }
    const decodedJwt = parseJwt(cookieInfo);
    if (decodedJwt.exp * 1000 < Date.now()) {
        return "";
    }
    return !!cookieInfo;
};
