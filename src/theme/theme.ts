import { PaletteMode } from "@mui/material";
import { createTheme } from "@mui/material/styles";
import breakpointsFunc from "./breakpoints";
import { colorsDark, colorsLight } from "./colors";
import { typographyStyle } from "./fonts";
import tooltip from "./tooltips";

const customTheme = (mode: PaletteMode | undefined) => {
    const dark = colorsDark();
    const light = colorsLight();
    const colors = mode === "dark" ? dark : light;
    const breakpoints: {} = breakpointsFunc();
    const typography: {} = typographyStyle();
    return () =>
        createTheme({
            breakpoints,
            palette: {
                mode,
                ...colors
            },
            typography,
            components: {
                // MuiButton: {
                //     ...muiButton(),
                // },
                MuiTextField: {
                    variants: [
                        {
                            props: {
                                color: "secondary"
                            },
                            style: {
                                "& .MuiOutlinedInput-root": {
                                    // "&:hover fieldset": {
                                    //     borderColor: colorsLight().primary.main, // - Set the Input border when parent has :hover
                                    // },
                                    "&.Mui-focused fieldset": {
                                        // - Set the Input border when parent is focused
                                        borderColor: colorsLight().secondary.main
                                    }
                                }
                            }
                        }
                    ]
                },

                MuiTooltip: {
                    ...tooltip()
                },

                MuiFormHelperText: {
                    styleOverrides: {
                        // Name of the slot
                        root: {
                            // Some CSS
                            fontSize: "0.75rem",
                            margin: "4px 0 0 0"
                        }
                    }
                }
            }
        });
};

export default customTheme;
