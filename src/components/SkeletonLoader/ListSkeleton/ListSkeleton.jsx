import { CustomSkeleton } from "@hipdevteam/afglobalcomponents";
import { Box, Grid } from "@mui/material";
import React from "react";

function ListSkeleton() {
    return (
        <Box>
            <Grid container spacing={1}>
                <Grid item xs={12} md={12}>
                    <Box
                        sx={{
                            width: 1,
                            padding: "30px 16px",
                            backgroundColor: "background.paper",
                            borderRadius: "4px",
                        }}
                    >
                        <CustomSkeleton variant="rectangular" height={42} width="100%" />
                        <br />
                        <CustomSkeleton variant="rectangular" height={42} width="100%" />
                        <br />
                        <CustomSkeleton variant="rectangular" height={42} width="100%" />
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
}

export default ListSkeleton;
