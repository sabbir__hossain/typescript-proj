import React from "react";
import "../../layouts/sidebar/core_sidebar.css";

const drawerWidth = 240;

// const openedMixin = (theme) => ({
//     width: drawerWidth,
//     transition: theme.transitions.create("width", {
//         easing: theme.transitions.easing.sharp,
//         duration: theme.transitions.duration.enteringScreen
//     }),
//     overflowX: "hidden"
// });

// const closedMixin = (theme) => ({
//     transition: theme.transitions.create("width", {
//         easing: theme.transitions.easing.sharp,
//         duration: theme.transitions.duration.leavingScreen
//     }),
//     overflowX: "hidden",
//     width: `calc(${theme.spacing(10)} + 1px)`,
//     [theme.breakpoints.up("sm")]: {
//         width: `calc(${theme.spacing(10)} + 1px)`
//     }
// });

// const Drawer = styled(MuiDrawer, {
//     shouldForwardProp: (prop) => prop !== "open"
// })(({ theme, open }) => ({
//     width: drawerWidth,
//     flexShrink: 0,
//     whiteSpace: "nowrap",
//     boxSizing: "border-box",
//     ...(open && {
//         ...openedMixin(theme),
//         "& .MuiDrawer-paper": openedMixin(theme)
//     }),
//     ...(!open && {
//         ...closedMixin(theme),
//         "& .MuiDrawer-paper": closedMixin(theme)
//     })
// }));
// const CustomizedDrawer = styled(Drawer)`
//     & .MuiDrawer-paper {
//         z-index: 0;
//     }
// `;

// function SideBarSkeleton({ sideBarStatus, customClass = null }) {
//     const defaultClasses = "schema_sidebar";

//     const classes = `${defaultClasses} ${customClass}`;
//     return (
//         <StyledEngineProvider injectFirst>
//             <Box className={classes}>
//                 <CustomizedDrawer className="main-sidebar" variant="permanent" open={sideBarStatus}>
//                     <Box className="logo_container">
//                         <CustomSkeleton variant="text" height={32} width="90%" />
//                     </Box>

//                     <List className="sidebar_menu">
//                         <ListItem>
//                             <CustomSkeleton variant="rectangular" height={32} width="100%" />
//                         </ListItem>

//                         <ListItem>
//                             <CustomSkeleton variant="rectangular" height={32} width="100%" />
//                         </ListItem>
//                         <ListItem>
//                             <CustomSkeleton variant="rectangular" height={32} width="100%" />
//                         </ListItem>
//                     </List>

//                     {/* Start user Logout */}

//                     <Box className="user_logout" sx={{ backgroundColor: "common.white" }}>
//                         <Box className="dark_toggle">
//                             <CustomSkeleton variant="rectangular" height={32} width="100%" />
//                         </Box>
//                         <Box className="user_logout" sx={{ backgroundColor: "background.paper" }}>
//                             <Box className="dark_toggle">
//                                 <CustomSkeleton variant="rectangular" height={32} width="100%" />
//                             </Box>
//                             <Box className="logout">
//                                 <Box>
//                                     <CustomSkeleton variant="circular" height={42} width={42} />
//                                 </Box>
//                                 <CustomSkeleton variant="rectangular" height={42} width={120} />
//                             </Box>
//                         </Box>
//                     </Box>
//                 </CustomizedDrawer>
//             </Box>
//         </StyledEngineProvider>
//     );
// }

function SideBarSkeleton() {
    return <>"ff"</>;
}

export default SideBarSkeleton;
