import { Box, ThemeProvider } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import storage from "local-storage-fallback";
import React, { useEffect, useMemo, useState } from "react";
import { Helmet, HelmetProvider } from "react-helmet-async";
import customTheme from "../../../theme/theme";
import ContentSkeleton from "./ContentSkeleton";

function MainPageSkeleton() {
    const [sideBarStatus, setSideBarStatus] = useState(true);
    const [mode, setMode] = useState("light");
    useEffect(() => {
        const modePostfix = storage.getItem("modePostfix");
        const themeMode = storage.getItem(`mode_${modePostfix}`);
        setMode(themeMode || "light");
    }, []);

    const theme = useMemo(customTheme(mode == "dark" ? "dark" : "light"), [mode]);
    return (
        <HelmetProvider>
            <Helmet defer={false}>
                <title />
            </Helmet>
            <ThemeProvider theme={theme}>
                <Box>
                    <CssBaseline />

                    {/* <TopBar setSideBarStatus={setSideBarStatus} sideBarStatus={sideBarStatus} /> */}

                    <Box sx={{ display: "flex", marginTop: "60px" }}>
                        {/* <SideBarSkeleton sideBarStatus={sideBarStatus} /> */}
                        <Box
                            style={{
                                width: "100%",
                                backgroundColor: "background.default"
                            }}
                        >
                            <ContentSkeleton />
                        </Box>
                    </Box>
                </Box>
            </ThemeProvider>
        </HelmetProvider>
    );
}

export default MainPageSkeleton;
