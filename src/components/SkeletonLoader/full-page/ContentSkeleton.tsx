import { CustomSkeleton } from "@hipdevteam/afglobalcomponents";
import { Box, Grid, Typography } from "@mui/material";
import React from "react";

function ContentSkeleton() {
    return (
        <Box sx={{ padding: "20px" }}>
            <Grid container spacing={2}>
                <Grid item xs={12} md={12}>
                    <Box sx={{ width: 1, mb: "30px" }}>
                        <Grid container>
                            <Grid item xs={8}>
                                <Typography component="span" variant="h5" color="text.fill">
                                    <CustomSkeleton variant="text" height={42} width="10%" />
                                </Typography>
                                <CustomSkeleton variant="text" height={42} width="20%" />
                            </Grid>
                            <Grid item xs={4} sx={{ alignSelf: "center" }}>
                                <Grid container sx={{ justifyContent: "flex-end" }}>
                                    <CustomSkeleton variant="rectangular" height={42} width="40%" />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                    <Box
                        sx={{
                            width: 1,
                            padding: "30px 16px",
                            backgroundColor: "background.paper",
                            borderRadius: "4px"
                        }}
                    >
                        <CustomSkeleton variant="rectangular" height={42} width="100%" />
                        <br />
                        <CustomSkeleton variant="rectangular" height={42} width="100%" />
                        <br />
                        <CustomSkeleton variant="rectangular" height={42} width="100%" />
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
}

export default ContentSkeleton;
