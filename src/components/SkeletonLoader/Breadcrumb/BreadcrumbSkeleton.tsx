import { CustomSkeleton } from "@hipdevteam/afglobalcomponents";
import { Box, Grid, Typography } from "@mui/material";
import React from "react";

function BreadrumSkeleton({ customStyle = "" }) {
    return (
        <Grid container className={customStyle}>
            <Grid item xs={8}>
                <Typography component="span" variant="h5" color="text.fill">
                    <CustomSkeleton variant="text" width="20%" />
                </Typography>
                <Box>
                    <CustomSkeleton variant="rectangular" width="40%" />
                </Box>
            </Grid>
        </Grid>
    );
}

export default BreadrumSkeleton;
