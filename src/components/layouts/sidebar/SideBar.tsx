// import { CustomButton, CustomTooltip } from "@hipdevteam/afglobalcomponents";
// import ArrowBackIcon from "@mui/icons-material/ArrowBack";
// import AssessmentIcon from "@mui/icons-material/Assessment";
// import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
// import PixIcon from "@mui/icons-material/Pix";
// import SellIcon from "@mui/icons-material/Sell";
// import {
//     Box,
//     Drawer,
//     List,
//     ListItem,
//     ListItemIcon,
//     ListItemText,
//     StyledEngineProvider,
//     Typography,
//     useTheme
// } from "@mui/material";
// import React, { useEffect, useState } from "react";
// import connect from "react-redux/es/components/connect";
// import { useNavigate } from "react-router-dom";
// import Icons from "../../../assets/Icons/Icons";
// import { SideBarProps } from "../../../interfaces/components/layouts/sidebar/SideBar";
// // import { getUserProfileData } from "../../../stateManagement/userProfileManage/userProfileManageAction";
// import * as CONSTANT from "../../../utils/Constant/GlobalConstant";
// import "./core_sidebar.css";

// function SideBar(props: SideBarProps) {
//     const {
//         drawerWidth,
//         mobileOpen,
//         setMobileOpen,
//         ThemeToggleButton,
//         customClass = null,
//         userProfileData,
//         appName,
//         featureAccess
//     } = props;
//     const accessToken: string = process.env.REACT_APP_ACCESS_TOKEN || "";
//     const refreshToken: string = process.env.REACT_APP_REFRESH_TOKEN || "";
//     const emailVerified: string = process.env.REACT_APP_EMAIL_VERIFIED_STATUS || "";
//     const redirectTo: string = process.env.REACT_APP_REDIRECT_TO || "";

//     const defaultClasses = "schema_sidebar";
//     let sideBar;
//     const navigate = useNavigate();
//     const [activeRoute, setActiveRoute] = useState("");
//     const [featureAccessData, setFeatureAccessData] = useState([]);
//     const theme = useTheme();
//     useEffect(() => {
//         // TODO SHOULD UPDATE
//         // setActiveRoute(navigate.pathname);
//     }, []);

//     // useEffect(() => {
//     //     getUserProfileData();
//     // }, []);

//     useEffect(() => {
//         setFeatureAccessData(featureAccess.data);
//     }, [featureAccess.data]);

//     const handleDrawerToggle = () => {
//         setMobileOpen(!mobileOpen);
//     };

//     const handleMenuClick = (route: string) => {
//         navigate(route);
//         setActiveRoute(route);
//     };

//     if (appName === CONSTANT.MAIN_SIDEBAR) {
//         sideBar = (
//             <>
//                 <Box className="goback_link" onClick={() => window.location.replace("/dashboard")}>
//                     <ArrowBackIcon />
//                     <CustomButton
//                         // title={<Typography variant="buttonMedium">Agency Dashboard</Typography>}
//                         // TODO SHOULD UPDATE
//                         title={<Typography>Agency Dashboard</Typography>}
//                         variant="text"
//                         size="medium"
//                     />
//                 </Box>
//                 <List className="sidebar_menu">
//                     <ListItem
//                         onClick={() => handleMenuClick("/ghl/location")}
//                         button
//                         className={activeRoute === "/ghl/location" ? "active" : ""}
//                     >
//                         <ListItemIcon sx={{ paddingLeft: "1rem" }}>
//                             <PeopleAltIcon
//                                 sx={{
//                                     color: "text.fill"
//                                 }}
//                             />
//                         </ListItemIcon>
//                         <ListItemText
//                             sx={{
//                                 marginTop: "2px",
//                                 marginBottom: "2px",
//                                 paddingLeft: "1rem",
//                                 color: "text.primary"
//                             }}
//                             primary={
//                                 <Typography component="span" variant="subtitle2">
//                                     Ghl Locations
//                                 </Typography>
//                             }
//                         />
//                     </ListItem>
//                     <ListItem
//                         onClick={() => handleMenuClick("/ghl/opportunity")}
//                         button
//                         className={activeRoute === "/ghl/opportunity" ? "active" : ""}
//                     >
//                         <ListItemIcon sx={{ paddingLeft: "1rem" }}>
//                             <SellIcon
//                                 sx={{
//                                     color: "text.fill"
//                                 }}
//                             />
//                         </ListItemIcon>
//                         <ListItemText
//                             sx={{
//                                 marginTop: "2px",
//                                 marginBottom: "2px",
//                                 paddingLeft: "1rem",
//                                 color: "text.primary"
//                             }}
//                             primary={
//                                 <Typography component="span" variant="subtitle2">
//                                     Opportunity
//                                 </Typography>
//                             }
//                         />
//                     </ListItem>

//                     <ListItem
//                         onClick={() => handleMenuClick("/ghl/tag-source-stage")}
//                         button
//                         className={activeRoute === "/ghl/tag-source-stage" ? "active" : ""}
//                     >
//                         <ListItemIcon sx={{ paddingLeft: "1rem" }}>
//                             <PixIcon
//                                 sx={{
//                                     color: "text.fill"
//                                 }}
//                             />
//                         </ListItemIcon>
//                         <ListItemText
//                             sx={{
//                                 marginTop: "2px",
//                                 marginBottom: "2px",
//                                 paddingLeft: "1rem",
//                                 color: "text.primary"
//                             }}
//                             primary={
//                                 <Typography component="span" variant="subtitle2">
//                                     Tag/Source/Stage
//                                 </Typography>
//                             }
//                         />
//                     </ListItem>
//                     <ListItem
//                         onClick={() => handleMenuClick("/ghl/opportunity-report")}
//                         button
//                         className={activeRoute === "/ghl/opportunity-report" ? "active" : ""}
//                     >
//                         <ListItemIcon sx={{ paddingLeft: "1rem" }}>
//                             <AssessmentIcon
//                                 sx={{
//                                     color: "text.fill"
//                                 }}
//                             />
//                         </ListItemIcon>
//                         <ListItemText
//                             sx={{
//                                 marginTop: "2px",
//                                 marginBottom: "2px",
//                                 paddingLeft: "1rem",
//                                 color: "text.primary"
//                             }}
//                             primary={
//                                 <Typography component="span" variant="subtitle2">
//                                     Opportunity Report
//                                 </Typography>
//                             }
//                         />
//                     </ListItem>
//                     <ListItem
//                         onClick={() => handleMenuClick("/ghl/api-auth")}
//                         button
//                         className={activeRoute === "/ghl/api-auth" ? "active" : ""}
//                     >
//                         <ListItemIcon sx={{ paddingLeft: "1rem" }}>
//                             {theme.palette.mode === "light"
//                                 ? Icons.ghlIconLight
//                                 : Icons.ghlIconDark}
//                         </ListItemIcon>
//                         <ListItemText
//                             sx={{
//                                 marginTop: "2px",
//                                 marginBottom: "2px",
//                                 paddingLeft: "1rem",
//                                 color: "text.primary"
//                             }}
//                             primary={
//                                 <Typography component="span" variant="subtitle2">
//                                     GHL Auth
//                                 </Typography>
//                             }
//                         />
//                     </ListItem>
//                 </List>
//             </>
//         );
//     }

//     const handleLogout = (e: React.ChangeEvent<HTMLInputElement>) => {
//         e.preventDefault();

//         // let formBody: any[] = [];
//         // formBody.push(`access_token=${getCookie(accessToken)}`);
//         // formBody.push(`refresh_token=${getCookie(refreshToken)}`);
//         // formBody = formBody.join("&");
//         // logout(formBody);
//         // setCookie(accessToken, "", "1 sec");
//         // setCookie(refreshToken, "", "1 sec");
//         // setOtherCookie(emailVerified, "");
//         // storage.setItem(redirectTo, "/dashboard");
//         // window.location.replace("/login");
//     };

//     const generateName = (firstName: string, lastName: string) => {
//         let name;
//         if (firstName !== "") {
//             name = firstName.substring(0, 1).toUpperCase();
//         }

//         if (lastName !== "") {
//             name += lastName.substring(0, 1).toUpperCase();
//         } else {
//             name = firstName.substring(0, 2).toUpperCase();
//         }

//         return name;
//     };

//     const classes = `${defaultClasses} ${customClass}`;

//     return (
//         <StyledEngineProvider injectFirst>
//             <Box
//                 component="nav"
//                 sx={{ width: { lg: drawerWidth }, flexShrink: { md: 0 }, zIndex: "1" }}
//                 aria-label="mailbox folders"
//                 className={classes}
//             >
//                 {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
//                 <Drawer
//                     variant="temporary"
//                     open={mobileOpen}
//                     className={classes}
//                     onClose={handleDrawerToggle}
//                     ModalProps={{
//                         keepMounted: true // Better open performance on mobile.
//                     }}
//                     sx={{
//                         display: { xs: "block", sm: "block", md: "block", lg: "none" },
//                         "& .MuiDrawer-paper": {
//                             boxSizing: "border-box",
//                             width: drawerWidth,
//                             backgroundImage: "none"
//                         }
//                     }}
//                 >
//                     <>
//                         {sideBar}

//                         <Box className="user_logout" sx={{ backgroundColor: "background.paper" }}>
//                             <Box className="dark_toggle">
//                                 <Typography variant="subtitle2">Dark Mode</Typography>
//                                 <ThemeToggleButton />
//                             </Box>
//                             <Box className="logout">
//                                 <Typography variant="h4">
//                                     {generateName(
//                                         userProfileData && userProfileData.first_name
//                                             ? userProfileData.first_name
//                                             : "",
//                                         userProfileData && userProfileData.last_name
//                                             ? userProfileData.last_name
//                                             : ""
//                                     )}
//                                 </Typography>
//                                 <CustomButton
//                                     title="Logout"
//                                     handleButton={handleLogout}
//                                     variant="text"
//                                     disableRipple
//                                     sx={{
//                                         color: "text.primary",
//                                         "&:hover": {
//                                             backgroundColor: "transparent"
//                                         }
//                                     }}
//                                 />
//                             </Box>
//                         </Box>
//                     </>
//                 </Drawer>

//                 <Drawer
//                     variant="permanent"
//                     sx={{
//                         display: { xs: "none", sm: "none", md: "none", lg: "block" },
//                         "& .MuiDrawer-paper": {
//                             boxSizing: "border-box",
//                             width: drawerWidth,
//                             marginTop: "64px",
//                             height: "calc(100% - 64px) !important"
//                         }
//                     }}
//                     open
//                 >
//                     <>
//                         {sideBar}

//                         <Box className="user_logout" sx={{ backgroundColor: "background.paper" }}>
//                             <Box className="dark_toggle">
//                                 <Typography variant="subtitle2">Dark Mode</Typography>
//                                 <ThemeToggleButton />
//                             </Box>
//                             <Box className="logout">
//                                 <CustomTooltip
//                                     placement="right-end"
//                                     title={
//                                         <Box sx={{ display: "flex", flexDirection: "column" }}>
//                                             <Typography variant="body2">
//                                                 Name:{" "}
//                                                 {userProfileData && userProfileData.first_name
//                                                     ? userProfileData.first_name
//                                                     : ""}
//                                             </Typography>
//                                             <Typography variant="body2">
//                                                 Email:{" "}
//                                                 {userProfileData && userProfileData.email
//                                                     ? userProfileData.email
//                                                     : ""}
//                                             </Typography>
//                                         </Box>
//                                     }
//                                 >
//                                     <Typography variant="h4">
//                                         {generateName(
//                                             userProfileData && userProfileData.first_name
//                                                 ? userProfileData.first_name
//                                                 : "",
//                                             userProfileData && userProfileData.last_name
//                                                 ? userProfileData.last_name
//                                                 : ""
//                                         )}
//                                     </Typography>
//                                 </CustomTooltip>
//                                 <CustomButton
//                                     title="Logout"
//                                     handleButton={handleLogout}
//                                     variant="text"
//                                     disableRipple
//                                     sx={{
//                                         color: "text.primary",
//                                         "&:hover": {
//                                             backgroundColor: "transparent"
//                                         }
//                                     }}
//                                 />
//                             </Box>
//                         </Box>
//                     </>
//                 </Drawer>
//             </Box>
//         </StyledEngineProvider>
//     );
// }

// // const mapStateToProps = (state) => ({
// //     userProfileData: state.userProfileManageReducer.userProfileData,
// //     featureAccess: state.featureAccessReducer
// // });
// // const mapDispatchToProps = (dispatch) => ({
// //     getUserProfileData: (params) => dispatch(getUserProfileData(params)),
// //     logout: (params) => dispatch(logout(params))
// // });
// export default connect(null, null)(SideBar);

import React from "react";

function SideBar() {
    return <div>SideBar</div>;
}

export default SideBar;
