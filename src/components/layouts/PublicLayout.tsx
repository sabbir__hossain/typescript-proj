// import { PaletteMode, ThemeProvider } from "@mui/material";
// import storage from "local-storage-fallback";
// import React, { useMemo, useState } from "react";
// import { Helmet, HelmetProvider } from "react-helmet-async";
// import customTheme from "../../theme/theme";

// interface PublicLayoutProps {
//     pageTitle: string;
//     children: React.ReactNode;
// }

// function PublicLayout(props: PublicLayoutProps) {
//     const { children, pageTitle } = props;
//     const themeMode = storage.getItem("mode");
//     const [mode, setMode] = useState<PaletteMode | undefined>(
//         themeMode == "dark" ? "dark" : "light"
//     );
//     const theme = useMemo(customTheme(mode), [mode]);

//     return (
//         <HelmetProvider>
//             {pageTitle && (
//                 <Helmet defer={false}>
//                     <title>{pageTitle}</title>
//                 </Helmet>
//             )}
//             <ThemeProvider theme={theme}>{children}</ThemeProvider>
//         </HelmetProvider>
//     );
// }

// export default PublicLayout;

import React from "react";

function PublicLayout() {
    return <div>PublicLayout</div>;
}

export default PublicLayout;
