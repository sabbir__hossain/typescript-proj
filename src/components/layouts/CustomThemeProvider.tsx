// import { PaletteMode } from "@mui/material";
// import { ThemeProvider } from "@mui/material/styles";
// import storage from "local-storage-fallback";
// import React, { useMemo, useState } from "react";
// import customTheme from "../../theme/theme";
// interface CustomThemeProviderPropsType {
//     children: React.ReactNode;
// }
// function CustomThemeProvider(props: CustomThemeProviderPropsType) {
//     const { children } = props;
//     const themeMode = storage.getItem("mode");
//     const [mode, setMode] = useState<PaletteMode | undefined>(
//         themeMode == "dark" ? "dark" : "light"
//     );

//     // window.changeChildColorMode = (colorMode) => {
//     //     setMode(storage.getItem("mode"));
//     // };

//     const theme = useMemo(customTheme(mode), [mode]);
//     return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
// }

// export default CustomThemeProvider;

import React from "react";

function CustomThemeProvider() {
    return <div>CustomThemeProvider</div>;
}

export default CustomThemeProvider;
