import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import {
    Box,
    IconButton,
    PaletteMode,
    StyledEngineProvider,
    ThemeProvider,
    useTheme
} from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import storage from "local-storage-fallback";
import React, { useContext, useEffect, useMemo, useState } from "react";
import { UserLayoutProps } from "../../interfaces/components/layouts/UserLayout";
import customTheme from "../../theme/theme";
import SideBar from "./sidebar/SideBar";
import "./style/userLayout_custom.css";
import TopBar from "./topbar/TopBar";

const drawerWidth = 240;
const ColorModeContext = React.createContext({
    toggleColorMode: () => {}
});

function ThemeToggleButton(): React.ReactNode {
    const theme = useTheme();
    const colorMode = useContext(ColorModeContext);
    return (
        <IconButton
            sx={{ ml: 1, marginLeft: "30px", color: "secondary" }}
            onClick={() => colorMode.toggleColorMode()}
        >
            {theme.palette.mode === "dark" ? (
                <Brightness7Icon color="secondary" />
            ) : (
                <Brightness4Icon color="primary" />
            )}
        </IconButton>
    );
}

function UserLayout(props: UserLayoutProps) {
    const {
        children,
        pageTitle,
        customClass = "",
        appName,
        haveAccess,
        userProfileData,
        featureAccess
    } = props;
    const [sideBarStatus, setSideBarStatus] = useState(true);
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [mode, setMode] = React.useState<PaletteMode | string>("light");

    useEffect(() => {
        const modePostfix = storage.getItem("modePostfix");
        const themeMode = storage.getItem(`mode_${modePostfix}`);
        setMode(themeMode || "light");
        // getUserProfileData();
    }, []);

    const colorMode = React.useMemo(
        () => ({
            toggleColorMode: () => {
                setMode((prevMode) => (prevMode === "light" ? "dark" : "light"));
                if (storage.getItem("modePostfix") !== undefined) {
                    storage.setItem(
                        `mode_${storage.getItem("modePostfix")}`,
                        mode === "light" ? "dark" : "light"
                    );
                }
            }
        }),
        [mode]
    );

    const theme = useMemo(customTheme(mode == "dark" ? "dark" : "light"), [mode]);
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        if (userProfileData && userProfileData.role !== undefined) {
            setIsLoading(false);
        }
    }, [userProfileData]);

    // Please assign your default style classes which are include in style file
    const defaultClasses = "user_layout";
    const classes = `${defaultClasses} ${customClass}`;

    const renderMain = () => {
        if (!isLoading) {
            return (
                <ColorModeContext.Provider value={colorMode}>
                    <ThemeProvider theme={theme}>
                        {haveAccess && (
                            <Box className={classes}>
                                <CssBaseline />
                                {/* <TopBar mobileOpen={mobileOpen} setMobileOpen={setMobileOpen} /> */}
                                <TopBar />
                                <Box sx={{ display: "flex", marginTop: "64px" }}>
                                    {/* <SideBar
                                            drawerWidth={drawerWidth}
                                            mobileOpen={mobileOpen}
                                            setMobileOpen={setMobileOpen}
                                            // sideBarStatus={sideBarStatus}
                                            ThemeToggleButton={ThemeToggleButton}
                                            appName={appName}
                                        /> */}
                                    <SideBar />
                                    <Box
                                        className="app__middle__global__wrapper"
                                        style={{
                                            // width: "100%",
                                            backgroundColor: "background.default"
                                        }}
                                    >
                                        {children}
                                    </Box>
                                </Box>
                            </Box>
                        )}
                    </ThemeProvider>
                </ColorModeContext.Provider>
            );
        }
        return "";
    };

    return <StyledEngineProvider injectFirst>{renderMain()}</StyledEngineProvider>;
}

export default UserLayout;
