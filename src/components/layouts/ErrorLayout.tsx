// import { PaletteMode } from "@mui/material";
// import StyledEngineProvider from "@mui/material/StyledEngineProvider";
// import { ThemeProvider } from "@mui/material/styles";
// import storage from "local-storage-fallback";
// import React, { useMemo, useState } from "react";
// import { Helmet, HelmetProvider } from "react-helmet-async";
// import customTheme from "../../theme/theme";
// import "./style/userLayout_custom.css";

// const ColorModeContext = React.createContext({
//     toggleColorMode: () => {}
// });

// interface ErrorLayoutProps {
//     pageTitle: string;
//     children: React.ReactNode;
// }

// function ErrorLayout(props: ErrorLayoutProps) {
//     const { pageTitle, children } = props;
//     const themeMode = storage.getItem("mode");

//     const [mode, setMode] = useState<PaletteMode | undefined>(
//         themeMode == "dark" ? "dark" : "light"
//     );

//     const colorMode = useMemo(
//         () => ({
//             toggleColorMode: () => {
//                 setMode((prevMode) => (prevMode === "light" ? "dark" : "light"));
//                 storage.setItem("mode", mode === "light" ? "dark" : "light");
//             }
//         }),
//         [mode]
//     );

//     const theme = useMemo(customTheme(mode), [mode]);

//     // Please assign your default style classes which are include in style file
//     const renderMain = () => (
//         <HelmetProvider>
//             {pageTitle && (
//                 <Helmet defer={false}>
//                     <title>{pageTitle}</title>
//                 </Helmet>
//             )}
//             <ColorModeContext.Provider value={colorMode}>
//                 <ThemeProvider theme={theme}>{children}</ThemeProvider>
//             </ColorModeContext.Provider>
//         </HelmetProvider>
//     );

//     return <StyledEngineProvider injectFirst>{renderMain()}</StyledEngineProvider>;
// }

// export default ErrorLayout;

import React from "react";

function ErrorLayout() {
    return <div>ErrorLayout</div>;
}

export default ErrorLayout;
