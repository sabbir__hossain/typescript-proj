import { CustomButton } from "@hipdevteam/afglobalcomponents";
import { Box, Typography } from "@mui/material";
import React from "react";
import { useHistory } from "react-router-dom";
import "./custom.style.css";

const ComponentNotFound = ({ message = "Component not Found!", goBackUrl = null }) => {
    const history = useHistory();
    const handleGoBack = () => {
        history.goBack();
    };
    return (
        <Box
            className="not-found-container"
            sx={{
                backgroundColor: "background.paper",
            }}
        >
            <Typography variant="h4" color="text.secondary" sx={{ marginBottom: "8px" }}>
                {message}
            </Typography>
            <CustomButton
                title="Go Back"
                handleButton={handleGoBack}
                variant="contained"
                color="info"
            />
        </Box>
    );
};

export default ComponentNotFound;
